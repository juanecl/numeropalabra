/*************************************************************/
// NumeroPalabra
// @author   Juane
/*************************************************************/
var division = 10;

function getUnidades(num) {
    var valor = $("#txtNum").val();
    switch (parseInt(num)) {
        case 1:
            //Si es 1, o 1 es el último número
            if ((parseInt(valor)) === 1) {
                return "Uno";
            } else if(parseInt(valor.charAt(valor.length - 1)) === 1) {
                return "uno";
            } else {
                return "un";
            }
        case 2:
            return "dos";
        case 3:
            return "tres";
        case 4:
            return "cuatro";
        case 5:
            return "cinco";
        case 6:
            return "seis";
        case 7:
            return "siete";
        case 8:
            return "ocho";
        case 9:
            return "nueve";
        default:
            return "";
    }
}

function getDecenas(num) {
    var decena = Math.floor(num / division);
    var u = num - (decena * division);

    switch (decena) {
        case 1:
            switch (u)
            {
                case 0:
                    return "diez";
                case 1:
                    return "once";
                case 2:
                    return "doce";
                case 3:
                    return "trece";
                case 4:
                    return "catorce";
                case 5:
                    return "quince";
                default:
                    return "dieci" + getUnidades(u);
            }
        case 2:
            switch (u)
            {
                case 0:
                    return "veinte";
                default:
                    return "veinti" + getUnidades(u);
            }
        case 3:
            return getDecyUni("treinta", u);
        case 4:
            return getDecyUni("cuarenta", u);
        case 5:
            return getDecyUni("cincuenta", u);
        case 6:
            return getDecyUni("sesenta", u);
        case 7:
            return getDecyUni("setenta", u);
        case 8:
            return getDecyUni("ochenta", u);
        case 9:
            return getDecyUni("noventa", u);
        case 0:
            return getUnidades(u);
    }
}

function getDecyUni(d, u) {
    if (u > 0)
        return d + " y " + getUnidades(u);
    return d;
}

function getCentenas(num) {
    var c, d, div;
    div = Math.pow(division, 2);
    c = Math.floor(num / div);
    d = num - (c * div);

    switch (c)
    {
        case 1:
            if (d > 0)
                return "ciento " + getDecenas(d);
            return "cien";
        case 5:
            if (d > 0)
                return "quinientos " + getDecenas(d);
            return "quinientos";
        case 7:
            if (d > 0)
                return "setecientos " + getDecenas(d);
            return "setecientos";
        case 9:
            if (d > 0)
                return "novecientos " + getDecenas(d);
            return "novecientos";
        default:
            if (getUnidades(c) !== "")
                return getUnidades(c) + "cientos " + getDecenas(d);
            else
                getDecenas(d);
    }

    return getDecenas(d);
}

function defineFin(num, divisor, strSingular, strPlural) {
    var cientos, resto, letras;
    cientos = Math.floor(num / divisor);
    resto = num - (cientos * divisor);
    letras = "";
    if (cientos > 0)
        if (cientos > 1)
            letras = getCentenas(cientos) + " " + strPlural;
        else
            letras = strSingular;
    if (resto > 0)
        letras += "";
    return letras;
}

function getMiles(num) {
    var divisor, resto, strMiles, strCentenas;
    divisor = Math.pow(division, 3);
    resto = num - Math.floor(num / divisor) * divisor;
    strMiles = defineFin(num, divisor, "mil", "mil");
    strCentenas = getCentenas(resto);
    if (strMiles === "")
        return strCentenas;

    return strMiles + " " + strCentenas;
}

function getMillones(num) {
    var divisor, resto, strMiles, strMillones;
    divisor = Math.pow(division, 6);
    resto = num - (Math.floor(num / divisor) * divisor);

    strMillones = defineFin(num, divisor, "un millón", "millones");
    strMiles = getMiles(resto);

    if (strMillones === "")
        return strMiles;

    return strMillones + " " + strMiles;
}
function getMilesMillones(num) {
    var divisor, miles, resto, strMillones, strMilesMillones;
    divisor = Math.pow(division, 9);
    miles = Math.floor(num / divisor);
    resto = num - (miles * divisor);
    strMilesMillones = defineFin(num, divisor, "mil millones", "mil millones");
    strMillones = getMillones(resto);

    if (strMilesMillones === "")
        return strMillones;

    return strMilesMillones + " " + strMillones;
}
function getBillones(num) {
    var divisor, resto, strMilesMillones, strBillones;
    divisor = Math.pow(division, 12);
    resto = num - (Math.floor(num / divisor) * divisor);
    strBillones = defineFin(num, divisor, "un billón", "billones");
    strMilesMillones = getMilesMillones(resto);

    if (strBillones === "")
        return strMilesMillones;

    return strBillones + " " + strMilesMillones;
}

function getMilesBillones(num) {
    var divisor, resto, strBillones, strMilesBillones;
    divisor = Math.pow(division, 15);
    resto = num - (Math.floor(num / divisor) * divisor);
    strMilesBillones = defineFin(num, divisor, "mil billones", "mil billones");
    strBillones = getBillones(resto);

    if (strMilesBillones === "")
        return strBillones;

    return strMilesBillones + " " + strBillones;
}

function NumeroPalabra(num) {
    var len = num.toString().length;
    switch (len) {
        case 1:
            var res = getUnidades(num);
            return res;
        case 2:
            return getDecenas(num);
        case 3:
            return getCentenas(num);
        default:
            if (len > 3 && len <= 6) {
                return getMiles(num);
            } else if (len > 6 && len <= 9) {
                return getMillones(num);
            } else if (len > 9 && len <= 12) {
                return getMilesMillones(num);
            } else if (len > 12 && len <= 15) {
                return getBillones(num);
            } else if (len > 15 && len <= 18) {
                return getMilesBillones(num);
            } else {
                return "Fuera de los límites";
            }
    }
}
$(document).ready(function() {
    $("#txtNum").attr("placeholder", "Ingresa un número.  En JS el máximo es: " + Number.MAX_VALUE + "");
    $("a#convertir").click(function(e) {
        e.preventDefault();
        var num = $("#txtNum").val();
        if (isNumber(num)) {
            $("#contenedor #num p").html(num); 
            $("#contenedor #resultado p").html(NumeroPalabra(num));
        } else {
            $("#contenedor #resultado p").html("El texto ingresado no es un número");
        }
    });
});

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}